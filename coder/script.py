__author__ = "Igor Klepuszewski"

import re
import random


class Coder():
    """
    A class implementing shuffle encoding and decoding functionality

    ...

    Methods
    -------
    __encode_logic(self, input):
        Private method encoding word using shuffling technique

    encode(self, input):
        Method encoding received input

    __areAnagrams(self, str1, str2):
        Private method checking if two strings are anagrams

    __decode_logic(self, input, wordList):
        Private decoding input word using wordList

    decode(self, input):
        Method decoding received input
    """

    def __encode_logic(self, input):
        """
        Private method encoding word using shuffling technique

        If word is encodable method will return tuple
        with encoded and original value,
        else only original will be returned.

        Parameters
        ----------
        input : str, required
            Word to encode

        Returns
        -------
        (encoded, input) : tuple
            if word has been encoded

        input : str
            if word has not been encoded
        """
        if len(input) > 4:
            # extract first letter, last one, and middle
            first = input[0]
            shuffle = input[1:-1]
            last = input[-1]
            # check if middle does not consists of the same letters
            if shuffle != len(shuffle) * shuffle[0]:
                test = shuffle
                # shuffle middle until its different than original
                while test == shuffle:
                    listed = list(shuffle)
                    random.shuffle(listed)
                    shuffle = ''.join(listed)
                # return combined first letter, middle and last letter
                # as second value return original word
                return first+shuffle+last, input
            else:
                return input
        # if word has exactly 4 letters, swap middle ones
        elif len(input) == 4:
            first = input[0]
            shuffle = input[1:-1]
            shuffle = shuffle[::-1]
            last = input[-1]
            return first+shuffle+last, input
        else:
            return input

    def encode(self, input):
        """
        Public method that creates weird text from input

        Parameters
        ----------
        input : str, required
            Text to be encoded

        Returns
        -------
        value : str
            Encoded text
        """
        # Create set of words to make sure that words are unique
        wordList = set()
        temp = input

        # Find words in text
        tokenize_re = re.compile(r'(\w+)', re.U)
        result = tokenize_re.finditer(temp)
        for res in result:
            word = res.group()

            # encode word
            encoded = self.__encode_logic(word)
            # if word has been encoded
            if type(encoded) is tuple:
                wordList.add(encoded[1])
                encoded = encoded[0]
            # swap original word in text for encoded one
            temp = temp[:res.span()[0]] + encoded + temp[res.span()[1]:]

        # sort set of words alphabetically case insensitive
        sorted_words = list(wordList)
        sorted_words.sort(key=str.casefold)

        return '\n-weird-\n'+temp+'\n-weird-\n'+" ".join(sorted_words)

    def __areAnagram(self, str1, str2):
        """
        Private method checking if two strings are anagrams

        Parameters
        ----------
        str1 : str, required
            First string to check
        str2 : str, required
            Second string to check

        Returns
        -------
        value : boolean
            if two strings are anagrams
        """
        str1 = str1.lower()
        str2 = str2.lower()
        # Get lengths of both strings
        n1 = len(str1)
        n2 = len(str2)

        # If lenght of both strings is not same, then
        # they cannot be anagram
        if n1 != n2:
            return False

        # Sort both strings
        str1 = sorted(str1)
        str2 = sorted(str2)

        # Compare sorted strings
        for i in range(0, n1):
            if str1[i] != str2[i]:
                return False
        return True

    def __decode_logic(self, input, wordList):
        """
        Private method decoding input word using wordList

        Parameters
        ----------
        input : str, required
            Word to decode

        wordList : List<str>, required
            List of words

        Returns
        -------
        value : str
            Returnes decoded word or if word cannot be decoded,
            original word
        """
        # check each word from the list
        for word in wordList:
            # if corner letters matches encoded corner letters,
            # and middle parts are anagrams
            if (
                input[0] == word[0] and
                input[-1] == word[-1] and
                self.__areAnagram(input[1:-1], word[1:-1])
            ):
                # return word from list
                return word
        # if decoded word has not been found, return original word
        return input

    def decode(self, input):
        """
        Public method that decodes weird text

        Parameters
        ----------
        input : str, required
            Weird text to be decoded

        Returns
        -------
        value : str
            Decoded weird text
        """
        # split input text using separator as magic value
        data = input.split('\n-weird-\n')
        # if input has not been properly splitted raise Value Error
        if len(data) != 3:
            raise ValueError(
                "Composite is not from encoder.")
        # create text to be decoded, and word list
        text = data[1]
        wordList = data[2].split(" ")

        temp = text

        # find words to be decoded
        tokenize_re = re.compile(r'(\w+)', re.U)
        result = tokenize_re.finditer(temp)
        for res in result:
            word = res.group()
            # decode word
            encoded = self.__decode_logic(word, wordList)

            # swap encoded word with decoded one
            temp = temp[:res.span()[0]] + encoded + temp[res.span()[1]:]
        return temp


if __name__ == "__main__":
    text = """This is a long loooong test sentence
    with some big (biiiig) test words!"""
    coder = Coder()
    res = coder.encode(text)
    print(res)
    print(coder.decode(res))
