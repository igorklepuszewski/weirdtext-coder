__author__ = "Igor Klepuszewski"

from script import Coder
import unittest


class TestScript(unittest.TestCase):
    coder = Coder()

    def test_encode_logic(self):
        self.assertEqual(self.coder._Coder__encode_logic(
            "abca"), ("acba", "abca"))
        self.assertEqual(self.coder._Coder__encode_logic(
            "abca"), ("acba", "abca"))
        self.assertEqual(self.coder._Coder__encode_logic(
            "ab"), "ab")
        self.assertEqual(self.coder._Coder__encode_logic("abc"), "abc")
        self.assertEqual(self.coder._Coder__encode_logic(
            "abbbbbbbbbbbbbbbc"), "abbbbbbbbbbbbbbbc")

    def test_areAnagram(self):
        self.assertTrue(self.coder._Coder__areAnagram("Night", "Thing"))
        self.assertTrue(self.coder._Coder__areAnagram("Bored", "Robed"))
        self.assertTrue(self.coder._Coder__areAnagram("Stressed", "Desserts"))
        self.assertFalse(self.coder._Coder__areAnagram("Stressed", "Dessert"))
        self.assertFalse(self.coder._Coder__areAnagram("Bored", "Robert"))
        self.assertFalse(self.coder._Coder__areAnagram("Night", "Thong"))

    def test_decode_logic(self):
        wordList = ["bus", "Denver", "sentence",
                    "Wrocław", "console", "workspace"]

        self.assertEqual(self.coder._Coder__decode_logic(
            "bus", wordList), "bus")
        self.assertEqual(self.coder._Coder__decode_logic(
            "cnolsoe", wordList), "console")
        self.assertEqual(self.coder._Coder__decode_logic(
            "Wcłroaw", wordList), "Wrocław")
        self.assertEqual(self.coder._Coder__decode_logic(
            "stenncee", wordList), "sentence")
        self.assertEqual(self.coder._Coder__decode_logic(
            "Dvener", wordList), "Denver")
        self.assertEqual(self.coder._Coder__decode_logic(
            "wspaorkce", wordList), "workspace")

        self.assertNotEqual(self.coder._Coder__decode_logic(
            "Euaql", wordList), "Equal")
        self.assertNotEqual(self.coder._Coder__decode_logic(
            "wspaorkce", wordList), "workspac")

        self.assertEqual(self.coder._Coder__decode_logic(
            "wspaorkce", list()), "wspaorkce")

    def test_decode(self):
        self.assertRaises(ValueError, self.coder.decode, "lfj")

    def test_encode(self):
        self.assertEqual(self.coder.encode("&a*a$ab^"),
                         '\n-weird-\n&a*a$ab^\n-weird-\n')
        self.assertEqual(self.coder.encode("%"),
                         '\n-weird-\n%\n-weird-\n')


if __name__ == "__main__":
    unittest.main(verbosity=2)
