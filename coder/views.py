from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .script import Coder
# Create your views here.


class EncoderView(APIView):

    def post(self, request):
        coder = Coder()
        try:
            output = coder.encode(request.data["input"])
        except ValueError:
            return Response(status=status.HTTP_409_CONFLICT)
        content = {"output": output}
        return Response(content, status=status.HTTP_200_OK)


class DecoderView(APIView):

    def post(self, request):
        coder = Coder()
        output = coder.decode(request.data["input"])
        content = {"output": output}
        return Response(content, status=status.HTTP_200_OK)
