[![pipeline status](https://gitlab.com/igorklepuszewski/weirdtext-coder/badges/master/pipeline.svg)](https://gitlab.com/igorklepuszewski/weirdtext-coder/commits/master)

# Prerequisites

- Python 3.9
- Pipenv

# How to run?


## Development


### Backend

```
$ pipenv install --dev
$ pipenv shell
$ python manage.py runserver
```

## Deploy
Using CI/CD such as Travis CI or GitLab CI/CD.

```
Heroku:
  image: ruby:latest
  stage: deploy
  before_script:
    - gem install dpl
  script:
    - dpl --provider=heroku --app=$HEROKU_APP_NAME --api-key=$HEROKU_API_KEY
  environment:
    name: master
    url: $HEROKU_APP_HOST
  only:
    - master
```

